def main():
    F = open('inp.txt', 'r')
    optcodes = F.read().split(',')
    F.close()
    commands = { 1:(add, 2), 2:(multi, 2), 3:(inp, 0), 4:(out, 1)}
    position = 0
    while position < len(optcodes):
        command = int(optcodes[position][-2:])
        if command == 99:
            break
        Pmodes = optcodes[position][:-2]
        numOptions = commands[command][1]
        while len(Pmodes) < int(numOptions)+1:
            Pmodes = "0"+Pmodes
        options = optcodes[position+1:position+numOptions+1]
        values = []
        for num, mode in zip(options, Pmodes[0:numOptions+1]):
            if mode == '0':
                values.append(int(optcodes[int(num)]))
            elif mode == '1':
                values.append(int(num))
        if command in [4]:
            commands[command][0](*values)
            position = position+numOptions+1
        else:
            if Pmodes[numOptions] == '0':
                storePos = int(optcodes[position+numOptions+1])
            elif Pmodes[numOptions] == '1':   
                storePos = int(Pmodes[numOptions])
            output = str(commands[command][0](*values))
            optcodes[storePos] = output
            print(optcodes[position+numOptions+2])
            print("command: "+str(command))
            print("values: "+str(values))
            print("output: "+str(output))
            print("storepos: "+str(storePos)+'\n')
            position = position+numOptions+2
    print(optcodes)

def add(*numbers):
    output = 0
    for x in numbers:
        output+=x
    return output

def multi(*numbers):
    output = numbers[0]
    for x in numbers[1:]:
        output*=x
    return output

def inp():
    return input()

def out(output):
    print(output)

if __name__ == '__main__':
    main()
