def fuelCalc(mass):
    fuel = int(int(mass)/3)-2
    if int(fuel) <= 0:
        return 0
    fuelForFuel = fuelCalc(fuel)
    return (fuel+fuelForFuel)

F = open('inputd2.txt', 'r')
masslist = list(F.read().split('\n'))
F.close()
output = []
print(masslist)
masslist.pop()
print(len(masslist))
for mass in masslist:
    fuel=fuelCalc(mass)
    output.append(fuel)
    print(len(output))
print(sum(output))
