def main():
    cross = set()
    intersections = []
    commandlist = []
    F = open('inp.txt', 'r')
    commandlist.append(F.read().split(','))
    F.close()
    F = open('inp2.txt', 'r')
    commandlist.append(F.read().split(','))
    F.close()
    directions = {"U": add, "D": sub, "R": add, "L": sub}
    directions2 = {"U": 0, "D":0, "R":1, "L":1}
    position = [0,0]
    for command in commandlist[0]:
        print(command)
        direction = command[0]
        movement = int(command[1:])
        function = directions[direction]
        axis = directions2[direction]
        for _ in range(movement):
            position[axis] = function(position[axis], 1)
            if tuple(position) not in cross:
                cross.add(tuple(position))
    position = [0, 0]
    for command in commandlist[1]:
        direction = command[0]
        movement = int(command[1:])
        function = directions[direction]
        axis = directions2[direction]
        for _ in range(movement):
            position[axis] = function(position[axis], 1)
            if tuple(position) in cross and tuple(position) not in intersections:
                intersections.append(tuple(position))
    print(intersections)
    Distances = list(map(manhattenDist, intersections))
    print(Distances)
    print(min(Distances))
                    
def manhattenDist(position):
    return abs(position[0])+abs(position[1])

def add(*numbers):
    output = 0
    for x in numbers:
        output+=x
    return output

def sub(*numbers):
    output = numbers[0]
    for x in numbers[1:]:
        output-=x
    return output
main()
