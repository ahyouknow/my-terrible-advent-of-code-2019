def main():
    lowest = 137683
    maximum = 596253
    inp = "0"
    dubs = ['11', '22', '33', '44', '55', '66', '77', '88', '99']
    passwords = 0
    while int(inp) <= maximum:
        for digit1 in range(1, 6):
            for digit2 in range(digit1, 10):
                for digit3 in range(digit2, 10):
                    for digit4 in range(digit3, 10):
                        for digit5 in range(digit4, 10):
                            for digit6 in range(digit5, 10):
                                inp = ''.join(list(map(str, [digit1, digit2, digit3, digit4, digit5, digit6]))) 
                                if int(inp) < lowest or int(inp) > maximum:
                                    break
                                for check in dubs:
                                    if check in inp:
                                        index = inp.index(check)
                                        if index-1 >= 0:
                                            if inp[index-1] == check[0]:
                                                continue
                                        if index+2 <= 5:
                                            if inp[index+2] == check[0]:
                                                continue
                                        passwords+=1
                                        print(inp)
                                        break
    print(passwords)
main()
