#!/bin/python3
def main():
    F = open('inp.txt', 'r')
    optcodes = F.read().split(',')
    F.close()
    optcodes = list(map(str, (map(int, optcodes))))
    for _ in range(100000):
        optcodes.append('0')
    commands = { 1:(add, 3), 2:(multi, 3), 3:(inp, 1), 4:(out, 1), 5:(jmpIfTrue, 2),
                6:(jmpIfFalse, 2), 7:(lessThan, 3), 8:(eql, 3), 9:(out, 1)}
    relativeBase = 0
    position = 0
    while position < len(optcodes):
        print("positon: "+str(position))
        print("relative Base: "+str(relativeBase))
        command = int(optcodes[position][-2:])
        print("Command: "+str(command))
        if command == 99:
            break
        Pmodes = optcodes[position][:-2]
        numOptions = commands[command][1]
        print('original Pmodes: '+Pmodes)
        while len(Pmodes) < numOptions-1:
            Pmodes = '0'+Pmodes
        if command == 4 and Pmodes == '':
            Pmodes = Pmodes+"0"
        elif command == 5 or command == 6 or command == 9:
            while len(Pmodes) < numOptions:
                Pmodes = '0'+Pmodes
        elif numOptions == 1 and "2" in Pmodes:
            Pmodes = "2"
        elif len(Pmodes) != numOptions:
            Pmodes = "1"+Pmodes
        Pmodes = Pmodes[::-1]
        print("Pmodes: "+str(Pmodes))
        optionValues = optcodes[position+1:position+numOptions+1]
        print("option Values: "+str(optionValues))
        values = []
        iteration=1
        for value, Pmode in zip(optionValues[0:numOptions], Pmodes):
            if Pmode == '0':
                values.append(int(optcodes[int(value)]))
            elif Pmode == '1':
                values.append(int(value))
            elif Pmode == '2':
                if command == 4 or command == 9 or command == 5 or command == 6:
                    values.append(int(optcodes[relativeBase+int(value)]))
                elif iteration == len(optionValues):
                    values.append(relativeBase+int(value))
                else:
                    values.append(int(optcodes[relativeBase+int(value)]))
            iteration+=1
        print("Values: "+str(values)+'\n')
        if command == 9:
            relativeBase+=int(values[0])
            print("new relative base: "+str(relativeBase))
            output = None
        else:
            output = commands[command][0](*values)
        if output == None:
            position = position+numOptions+1
        elif output == 'True':
            position = values[1]
        elif output == 'False':
            position = position+numOptions+1
        else:
            if values[-1] > len(optcodes):
                for _ in range(values[-1]):
                    optcodes.append('0')
            optcodes[values[-1]] = str(output)
            position = position+numOptions+1

def add(*numbers):
    return numbers[0]+numbers[1]

def multi(*numbers):
    return numbers[0]*numbers[1]

def inp(*inpu):
    return input('input: ')

def out(*output):
    print('output: '+str(output[0]))
    return None

def jmpIfTrue(*numbers):
    if numbers[0] != 0:
        return 'True'
    else:
        return 'False'

def jmpIfFalse(*numbers):
    if numbers[0] == 0:
        return 'True'
    else:
        return 'False'

def lessThan(*numbers):
    if numbers[0] < numbers[1]:
        return 1
    else:
        return 0

def eql(*numbers):
    if numbers[0] == numbers[1]:
        return 1
    else:
        return 0

if __name__ == '__main__':
    main()
