def main():
    F = open('inp.txt', 'r')
    orbits = F.read().split('\n')
    F.close()
    orbitMap = {}
    orbits.pop()
    for orbit in orbits:
        mass1, mass2 = orbit.split(')')
        orbitMap[mass2] = mass1
    SANorbit = orbitMap["SAN"]
    YOUorbit = orbitMap["YOU"]
    SANorbitList = []
    while SANorbit != 'COM':
        SANorbit = orbitMap[SANorbit]
        SANorbitList.append(SANorbit)
    YOUorbitList = []
    while YOUorbit != 'COM':
        YOUorbit = orbitMap[YOUorbit]
        YOUorbitList.append(YOUorbit)
        if YOUorbit in SANorbitList:
            break
    index = SANorbitList.index(YOUorbit)
    print(len(YOUorbitList)+index+1)
    
        

main()
