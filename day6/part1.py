def main():
    F = open('inp.txt', 'r')
    orbits = F.read().split('\n')
    F.close()
    orbitMap = {}
    orbits.pop()
    for orbit in orbits:
        mass1, mass2 = orbit.split(')')
        orbitMap[mass2] = mass1
    numOrbits = 0
    for mass in orbitMap.keys():
        while mass != 'COM':
            mass = orbitMap[mass]
            numOrbits+=1
    print(numOrbits)

main()
