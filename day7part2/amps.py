#!/bin/python3
import everyPossibleCombo

def main():
    everyPhase = everyPossibleCombo.main() 
    allResults = []
    for phaseSetting in everyPhase:
        amps = []
        for _ in range(5):
            amps.append(intcodeMachine())
        result = amps[0].run([phaseSetting[0], 0])
        for phase, amp in zip(phaseSetting[1:], amps[1:]):
            result = amp.run([phase, result])
        amp = -1
        results = []
        while amps[-1].command != 99:
            amp+=1
            amp = amp%5
            result = amps[amp].run([result])
            results.append(result)
        for amp in amps:
            del(amp)
        result = results[-6]
        allResults.append(result)
    print(max(allResults))
    
class intcodeMachine:
    def __init__(self):
        F = open('inp.txt', 'r')
        optcodes = F.read().split(',')
        F.close()
        self.optcodes = list(map(str, (map(int, optcodes))))
        self.commands = { 1:(add, 3), 2:(multi, 3), 3:(inp, 1), 4:(out, 1), 5:(jmpIfTrue, 2),
                6:(jmpIfFalse, 2), 7:(lessThan, 3), 8:(eql, 3)}
        self.position = 0

    def run(self, Input):
        InputCount = 0
        while self.position < len(self.optcodes):
            self.command = int(self.optcodes[self.position][-2:])
            if self.command == 99:
                break
            Pmodes = self.optcodes[self.position][:-2]
            numOptions = self.commands[self.command][1]
            while len(Pmodes) < numOptions-1:
                Pmodes = '0'+Pmodes
            if self.command == 4 and Pmodes == '':
                Pmodes = Pmodes+"0"
            elif self.command == 5 or self.command == 6:
                while len(Pmodes) < numOptions:
                    Pmodes = '0'+Pmodes
            else:
                Pmodes = "1"+Pmodes
            Pmodes = Pmodes[::-1]
            optionValues = self.optcodes[self.position+1:self.position+numOptions+1]
            values = []
            for value, Pmode in zip(optionValues[0:numOptions], Pmodes):
                if Pmode == '0':
                    values.append(int(self.optcodes[int(value)]))
                elif Pmode == '1':
                    values.append(int(value))
            output = self.commands[self.command][0](*values)
            if self.command == 3:
                #print(Input[InputCount])
                self.optcodes[values[-1]] = Input[InputCount]
                self.position = self.position+numOptions+1
                InputCount+=1
            elif self.command == 4:
                self.position = self.position+numOptions+1
                return values[0]
            elif output == 'True':
                self.position = values[1]
            elif output == 'False':
                self.position = self.position+numOptions+1
            else:
                self.optcodes[values[-1]] = str(output)
                self.position = self.position+numOptions+1

def add(*numbers):
    return numbers[0]+numbers[1]

def multi(*numbers):
    return numbers[0]*numbers[1]

def inp(*inpu):
    return None

def out(*output):
    #print(str(output[0]))
    return None

def jmpIfTrue(*numbers):
    if numbers[0] != 0:
        return 'True'
    else:
        return 'False'

def jmpIfFalse(*numbers):
    if numbers[0] == 0:
        return 'True'
    else:
        return 'False'

def lessThan(*numbers):
    if numbers[0] < numbers[1]:
        return 1
    else:
        return 0

def eql(*numbers):
    if numbers[0] == numbers[1]:
        return 1
    else:
        return 0

main()
