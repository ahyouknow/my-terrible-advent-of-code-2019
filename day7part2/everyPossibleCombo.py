def main():
    allPossiblePhases = calcPhases([])
    return allPossiblePhases

def calcPhases(usedphases):
    usedphases = usedphases.copy()
    if len(usedphases) == 5:
        return [usedphases]
    possiblePhases = list(range(5,10))
    for phase in usedphases:
        possiblePhases.remove(phase)
    biglist = []
    for number in possiblePhases:
        if number != possiblePhases[0]:
            usedphases.pop()
        if number in usedphases:
            continue
        else:
            usedphases.append(number)
            possibilities = calcPhases(usedphases)
            for possibility in possibilities:
                biglist.append(possibility)
    return biglist

if __name__ == '__main__':
    output = main()
    print(output)
    
