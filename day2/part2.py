def main():
    F = open('inp.txt', 'r')
    optcodes = F.read().split(',')
    F.close()
    optcodes = list(map(int, optcodes))
    for noun in range(100):
        optcodes[1] = noun
        for verb in range(100):
            optcodes[2] = verb
            output = optcodes.copy()
            output = execOpt(output)
            if output[0] == 19690720:
                print('noun:{}, verb:{}'.format(noun, verb))

def execOpt(optcodes):
    for position in range(0, len(optcodes)+1, 4):
        if optcodes[position] == 99:
            break
        else:
            command = optcodes[position]
            input1 = optcodes[position+1]
            input2 = optcodes[position+2]
            optPosition = optcodes[position+3]
            if command == 1:
                output = optcodes[input1]+optcodes[input2]
            elif command == 2:
                output = optcodes[input1]*optcodes[input2]
            optcodes[optPosition] = output
    return optcodes

main()
