F = open('inp.txt', 'r')
optcodes = F.read().split(',')
F.close()
optcodes = list(map(int, optcodes))
optcodes[1] = 12
optcodes[2] = 2
for position in range(0, len(optcodes)+1, 4):
    if optcodes[position] == 99:
        break
    else:
        command = optcodes[position]
        input1 = optcodes[position+1]
        input2 = optcodes[position+2]
        optPosition = optcodes[position+3]
        print(optPosition)
        if command == 1:
            output = optcodes[input1]+optcodes[input2]
        elif command == 2:
            output = optcodes[input1]*optcodes[input2]
        optcodes[optPosition] = output
print(optcodes)
