def main():
    width=25
    height=6
    f = open('inp.txt', 'r')
    inp = list(f.read())
    f.close()
    inp.pop()
    inp = list(map(int, inp))
    lengthOfLayer = width*height
    start = 0
    end = 0
    layers = []
    while end != len(inp):
        print(end)
        end+=lengthOfLayer
        layers.append(inp[start:end])
        start=end
    zeros = []
    for layer in layers:
        zeros.append(layer.count(0))
    minimumZeros = min(zeros)
    print(zeros)
    print(minimumZeros)
    index = zeros.index(minimumZeros)
    print(index)
    print(zeros[index])
    theLayer = layers[index]
    print(theLayer)
    print(len(theLayer))
    result = theLayer.count(1)*theLayer.count(2)
    print(result)
        

main()
