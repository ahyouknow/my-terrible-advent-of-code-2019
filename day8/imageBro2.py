def main():
    width=25
    height=6
    f = open('inp.txt', 'r')
    inp = list(f.read())
    f.close()
    inp.pop()
    inp = list(map(int, inp))
    lengthOfLayer = width*height
    start = 0
    end = 0
    layers = []
    while end != len(inp):
        end+=lengthOfLayer
        layers.append(inp[start:end])
        start=end
    output = ''
    for position in range(len(layers[0])):
        if position % width == 0:
            output += '\n'
        for layer in layers:
            color = layer[position]
            if color != 2:
                if color == 0:
                    output += ' '
                else:
                    output += '1'
                break
    output = list(output)
    output.remove('\n')
    print(output)
    print(output.index('\n'))
    print(''.join(output))
main()
