#!/bin/python3 

inp = open('results.txt', 'r')
results = inp.read().split('\n')
inp.close()
results.pop()
resultsDict={}
for result in results:
    result = result.split(' , ')
    resultsDict[int(result[0])] = int(result[1])
best = max(list(resultsDict.keys()))
print(best)
