#!/bin/bash

truncate -s 0 results.txt
readarray -t phases < possibilities.txt
for phase in ${phases[@]}
do
	secondCommand=0
	for number in {1..5}; do
		command=$(echo $phase | cut -c $number)
		secondCommand=$( { echo $command; echo $secondCommand; } | ./intcode.py)
	done
	echo "$secondCommand , $phase" >> results.txt
done		
./getBestResults.py
