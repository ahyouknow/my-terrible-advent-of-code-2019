#!/bin/python3
def main():
    F = open('inp.txt', 'r')
    optcodes = F.read().split(',')
    F.close()
    optcodes = list(map(str, (map(int, optcodes))))
    commands = { 1:(add, 3), 2:(multi, 3), 3:(inp, 1), 4:(out, 1), 5:(jmpIfTrue, 2),
                6:(jmpIfFalse, 2), 7:(lessThan, 3), 8:(eql, 3)}
    position = 0
    while position < len(optcodes):
        command = int(optcodes[position][-2:])
        if command == 99:
            break
        Pmodes = optcodes[position][:-2]
        numOptions = commands[command][1]
        while len(Pmodes) < numOptions-1:
            Pmodes = '0'+Pmodes
        if command == 4 and Pmodes == '':
            Pmodes = Pmodes+"0"
        elif command == 5 or command == 6:
            while len(Pmodes) < numOptions:
                Pmodes = '0'+Pmodes
        else:
            Pmodes = "1"+Pmodes
        Pmodes = Pmodes[::-1]
        optionValues = optcodes[position+1:position+numOptions+1]
        values = []
        for value, Pmode in zip(optionValues[0:numOptions], Pmodes):
            if Pmode == '0':
                values.append(int(optcodes[int(value)]))
            elif Pmode == '1':
                values.append(int(value))
        output = commands[command][0](*values)
        if output == None:
            position = position+numOptions+1
        elif output == 'True':
            position = values[1]
        elif output == 'False':
            position = position+numOptions+1
        else:
            optcodes[values[-1]] = str(output)
            position = position+numOptions+1

def add(*numbers):
    return numbers[0]+numbers[1]

def multi(*numbers):
    return numbers[0]*numbers[1]

def inp(*inpu):
    return input()

def out(*output):
    print(str(output[0]))
    return None

def jmpIfTrue(*numbers):
    if numbers[0] != 0:
        return 'True'
    else:
        return 'False'

def jmpIfFalse(*numbers):
    if numbers[0] == 0:
        return 'True'
    else:
        return 'False'

def lessThan(*numbers):
    if numbers[0] < numbers[1]:
        return 1
    else:
        return 0

def eql(*numbers):
    if numbers[0] == numbers[1]:
        return 1
    else:
        return 0

if __name__ == '__main__':
    main()
